#include <boost/python.hpp>
#include <iostream>
#include <string>

/// *** wyznaczanie gracza z najmocniejszym ukladem *** ///
boost::python::object findWinner(boost::python::list players, boost::python::list tableCards)
{
	int n = boost::python::extract<int>(players.attr("__len__")());


	/// *** wlasciwa implementacja zostanie wykonana pozniej *** ///


	/// *** przykladowy kod na zakonczenie, zwracany bedzie gracz *** ///
	/// *** ktory mial najsilniejszy uklad, ewentualnie kilku graczy ***///
	
	std::string winnerName;
	int i = 0;
	while(i < n)
	{
		std::string name = boost::python::extract<std::string>
		(players[i].attr("name"));
		if(name == winnerName)
			break;
	}
	return players[i];

	///* przy wiekszej liczbie graczy moze byc wiecej zwyciezcow, w tym *//
	///*** przypadku nalezaloby zmodyfikowac koncowke *** ///
}

BOOST_PYTHON_MODULE(cppFindWinner)
{
	boost::python::def("find_winner",findWinner);
}
