#include <boost/python.hpp>
#include <iostream>
#include <string>

/// *** kalkulator sily reki - biorac pod uwage karty gracza oraz   *** ///
/// *** karty na stole funkcja zwraca liczbe d 0 do 100 okreslajaca *** ///
/// *** procentowa szanse gracza na wygranie 			    *** ///
boost::python::object calculateHandForce(boost::python::list list)
{
	/// *** tu nastepuje konwersja danych z python do C++ *** ///
	int n = boost::python::extract<int>(list.attr("__len__")());
        std::pair<std::string,std::string> cards[n];	
	for(int i = 0; i < n; i++)
	{
		cards[i].first = boost::python::extract<std::string>
		(list[i].attr("face"));
		cards[i].second = boost::python::extract<std::string>
		(list[i].attr("suit"));
	}

	/// *** tu nastepuja wlasciwe obliczenia, pomocniczo *** ///
	/// *** wywolywane beda inne funkcje napisane w C++  *** ///

	/// *** test poprawnego wczytywania danych *** ///
	for(int i = 0; i < n; i++)
		std::cout<<cards[i].first<<"  "<<cards[i].second<<std::endl;

	/// *** funkcja w skonczonej implementacji bedzie zwracala wynik***///
	
	int wynik = 50;
	boost::python::object pyObject(wynik);
	return pyObject;
}

BOOST_PYTHON_MODULE(cppCalculate)
{
	boost::python::def("hand_force", calculateHandForce);
}



